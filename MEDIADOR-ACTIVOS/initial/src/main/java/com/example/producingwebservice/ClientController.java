package com.example.producingwebservice;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.spring.guides.gs_producing_web_service.GetActivoResponse;

@RestController
@RequestMapping("/activos")
public class ClientController {

	@Autowired
	ActivoClient client;

	@GetMapping(value = "/{activo}", produces = MediaType.APPLICATION_JSON_VALUE)
	public GetActivoResponse findActivoByString(@PathVariable("activo") String activo) {
		
	

		return client.getActivo(activo);

	}

}
