package com.example.producingwebservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import io.spring.guides.gs_producing_web_service.GetActivoRequest;
import io.spring.guides.gs_producing_web_service.GetActivoResponse;

public class ActivoClient extends WebServiceGatewaySupport {

	private static final Logger log = LoggerFactory.getLogger(ActivoClient.class);

	public GetActivoResponse getActivo(String activo) {

		GetActivoRequest request = new GetActivoRequest();
		request.setPlaca(activo);

		log.info("Requesting location for " + activo);

		return (GetActivoResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:9001/ws/activos.wsdl",
				request, new SoapActionCallback("http://spring.io/guides/gs-producing-web-service/GetActivoRequest"));
	}

}
