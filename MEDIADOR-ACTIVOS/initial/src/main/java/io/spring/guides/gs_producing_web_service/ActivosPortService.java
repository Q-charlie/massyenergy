
package io.spring.guides.gs_producing_web_service;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.3.2
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ActivosPortService", targetNamespace = "http://spring.io/guides/gs-producing-web-service", wsdlLocation = "http://localhost:9001/ws/activos.wsdl")
public class ActivosPortService
    extends Service
{

    private final static URL ACTIVOSPORTSERVICE_WSDL_LOCATION;
    private final static WebServiceException ACTIVOSPORTSERVICE_EXCEPTION;
    private final static QName ACTIVOSPORTSERVICE_QNAME = new QName("http://spring.io/guides/gs-producing-web-service", "ActivosPortService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:9001/ws/activos.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        ACTIVOSPORTSERVICE_WSDL_LOCATION = url;
        ACTIVOSPORTSERVICE_EXCEPTION = e;
    }

    public ActivosPortService() {
        super(__getWsdlLocation(), ACTIVOSPORTSERVICE_QNAME);
    }

    public ActivosPortService(WebServiceFeature... features) {
        super(__getWsdlLocation(), ACTIVOSPORTSERVICE_QNAME, features);
    }

    public ActivosPortService(URL wsdlLocation) {
        super(wsdlLocation, ACTIVOSPORTSERVICE_QNAME);
    }

    public ActivosPortService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, ACTIVOSPORTSERVICE_QNAME, features);
    }

    public ActivosPortService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ActivosPortService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns ActivosPort
     */
    @WebEndpoint(name = "ActivosPortSoap11")
    public ActivosPort getActivosPortSoap11() {
        return super.getPort(new QName("http://spring.io/guides/gs-producing-web-service", "ActivosPortSoap11"), ActivosPort.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ActivosPort
     */
    @WebEndpoint(name = "ActivosPortSoap11")
    public ActivosPort getActivosPortSoap11(WebServiceFeature... features) {
        return super.getPort(new QName("http://spring.io/guides/gs-producing-web-service", "ActivosPortSoap11"), ActivosPort.class, features);
    }

    private static URL __getWsdlLocation() {
        if (ACTIVOSPORTSERVICE_EXCEPTION!= null) {
            throw ACTIVOSPORTSERVICE_EXCEPTION;
        }
        return ACTIVOSPORTSERVICE_WSDL_LOCATION;
    }

}
