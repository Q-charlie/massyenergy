package com.example.producingwebservice;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;




@SpringBootApplication
public class ProducingWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducingWebServiceApplication.class, args);
	}
	
//	@Bean
//	  CommandLineRunner lookup(PersonaClient quoteClient) {
//	    return args -> {
//	      String country = "Spain";
//
//	      if (args.length > 0) {
//	        country = args[0];
//	      }
//	      GetCountryResponse response = quoteClient.getCountry(country);
//	      System.err.println(response.getCountry().getCurrency());
//	    };
//	  }

}
