package com.example.producingwebservice;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.spring.guides.gs_producing_web_service.GetPersonaResponse;

@RestController
@RequestMapping("/personas")
public class ClientController {

	@Autowired
	PersonaClient client;

	@GetMapping(value = "/{cedula}", produces = MediaType.APPLICATION_JSON_VALUE)
	public GetPersonaResponse findPersonaByString(@PathVariable("cedula") String cedula) {
		
	

		return client.getPersona(cedula);

	}

}
