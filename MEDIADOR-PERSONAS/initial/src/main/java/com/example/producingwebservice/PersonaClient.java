package com.example.producingwebservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import io.spring.guides.gs_producing_web_service.GetPersonaRequest;
import io.spring.guides.gs_producing_web_service.GetPersonaResponse;

public class PersonaClient extends WebServiceGatewaySupport {

	private static final Logger log = LoggerFactory.getLogger(PersonaClient.class);

	public GetPersonaResponse getPersona(String persona) {

		GetPersonaRequest request = new GetPersonaRequest();
		request.setCedula(persona);

		log.info("Requesting location for " + persona);

		return (GetPersonaResponse) getWebServiceTemplate().marshalSendAndReceive(
				"http://localhost:8081/ws/personas.wsdl", request,
				new SoapActionCallback("http://spring.io/guides/gs-producing-web-service/GetPersonaRequest"));
	}

}
