package com.example.producingwebservice;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import io.spring.guides.gs_producing_web_service.Activo;
import jakarta.annotation.PostConstruct;

@Component
public class ActivosRepository {
	
	private static final Map<String, Activo> activos = new HashMap<>();

	@PostConstruct // proporciona una data que se inyecta en el backbean
					// y despues se puede llamar para utilizarla

	public void initData() {

		/*
		 * El objeto activo cuenta con los siguientes campos: cedula, nombre, estado,
		 * area
		 */

		Activo carro = new Activo();
		carro.setPlaca("AAA-001");
		carro.setTipo("Automóvil - Ferrari Diablo");
		carro.setEstado("TRUE");

		activos.put(carro.getPlaca(), carro);
		
		Activo carro1 = new Activo();
		carro1.setPlaca("AAA-002");
		carro1.setTipo("Automóvil - Mercedes");
		carro1.setEstado("TRUE");
		
		activos.put(carro1.getPlaca(), carro1);

		Activo carro2 = new Activo();	
		carro2.setPlaca("AAA-003");
		carro2.setTipo("Automóvil - Mazda 323");
		carro2.setEstado("TRUE");
		
		activos.put(carro2.getPlaca(), carro2);

	}
	
	public Activo findActivo(String cedula) {
		Assert.notNull(cedula, "no se encuentra");
		return activos.get(cedula);
	}

}
