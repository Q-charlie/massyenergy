package com.example.producingwebservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import io.spring.guides.gs_producing_web_service.GetActivoRequest;
import io.spring.guides.gs_producing_web_service.GetActivoResponse;

@Endpoint
public class ActivosEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

	private ActivosRepository activosRepository;

	@Autowired
	public ActivosEndpoint(ActivosRepository activosRepository) {
		this.activosRepository = activosRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getActivoRequest")
	@ResponsePayload
	public GetActivoResponse getActivo(@RequestPayload GetActivoRequest request) {
		GetActivoResponse response = new GetActivoResponse();
		response.setActivo(activosRepository.findActivo(request.getPlaca()));

		return response;
	}


	
}
