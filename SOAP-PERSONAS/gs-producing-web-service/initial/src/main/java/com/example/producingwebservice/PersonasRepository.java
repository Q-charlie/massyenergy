package com.example.producingwebservice;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import io.spring.guides.gs_producing_web_service.Persona;
import jakarta.annotation.PostConstruct;

@Component
public class PersonasRepository {
	
	private static final Map<String, Persona> personas = new HashMap<>();

	@PostConstruct // proporciona una data que se inyecta en el backbean
					// y despues se puede llamar para utilizarla

	public void initData() {  

		/*
		 * El objeto persona cuenta con los siguientes campos: cedula, nombre, estado,
		 * area
		 */

		Persona andres = new Persona();
		andres.setCedula("1");
		andres.setNombre("Andrés Escobar");
		andres.setEstado("TRUE");
		andres.setArea("Comercial");

		personas.put(andres.getCedula(), andres);
		
		
		Persona pedro = new Persona();
		pedro.setCedula("2");
		pedro.setNombre("Pedro MArcial");
		pedro.setEstado("TRUE");
		pedro.setArea("Recursos Humanos");

		personas.put(pedro.getCedula(), pedro);
		
		Persona juan = new Persona();
		juan.setCedula("3"); 
		juan.setNombre("Juan Alberto Rodriguez");
		juan.setEstado("TRUE");
		juan.setArea("Comercial");

		personas.put(juan.getCedula(), juan);
	}
	
	public Persona findPersona(String cedula) {
		Assert.notNull(cedula, "no se encuentra");
		return personas.get(cedula);
	}

}
