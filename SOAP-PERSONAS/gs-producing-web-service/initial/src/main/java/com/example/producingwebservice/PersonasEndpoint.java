package com.example.producingwebservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import io.spring.guides.gs_producing_web_service.GetPersonaRequest;
import io.spring.guides.gs_producing_web_service.GetPersonaResponse;

@Endpoint
public class PersonasEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

	private PersonasRepository personasRepository;

	@Autowired
	public PersonasEndpoint(PersonasRepository personasRepository) {
		this.personasRepository = personasRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPersonaRequest")
	@ResponsePayload
	public GetPersonaResponse getPersona(@RequestPayload GetPersonaRequest request) {
		GetPersonaResponse response = new GetPersonaResponse();
		response.setPersona(personasRepository.findPersona(request.getCedula()));

		return response;
	}


	
}
